all:
	platformio -f -c vim run

upload:
	platformio -f -c vim run --target upload

clean:
	platformio -f -c vim run --target clean

program:
	platformio -f -c vim run --target fuses
	platformio -f -c vim run --target bootloader
	platformio -f -c vim run --target program

fuses:
	platformio -f -c vim run --target fuses

bootloader:
	platformio -f -c vim run --target fuses
	platformio -f -c vim run --target bootloader

uploadfs:
	platformio -f -c vim run --target uploadfs

update:
	platformio -f -c vim update

monitor:
	platformio -f -c vim device monitor
	

